This repo contains scripts for installing common apps I use.
 
## Setup
Install git
```
sudo apt-get install git
```
Get the scripts
```
git clone https://bitbucket.org/rhubot/install_scripts.git ~/install_scripts
```
Configure the environment
```
cd ~/install_scripts
./configure_install_scripts.sh
source ~/.bashrc
```
Note: `configure_install_scripts.sh` will set an environment variable via the `.bashrc` file. This allows the user to clone install_scripts to any location and other scripts are able to find the desired install scripts. The `configure_install_scripts.sh` script looks at the current directory when setting this variable, so it is required that the user is in the base of the install_scripts directory when that script is called.


## Usage
Each script can be executed on its own. Some scripts take command line arguments, which can be viewed using the `--help` flag or by manually inspecting the contents of the script.

Executing several scripts at the same time has been facilitated through the execution of the `install_dev_apps.sh` script, which provides a text-based menu to choose which install scripts to execute.
