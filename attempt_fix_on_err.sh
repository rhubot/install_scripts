#!/bin/bash -i


# Source .bashrc
set +e
#trap - ERR
source ~/.bashrc
set -e
#trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR


# Get color definitions
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# Print a large divider so the user knows an error occured
echo -e ""
for (( colIndex=0; colIndex<${COLUMNS}; colIndex++ ))
do
    printf "${ERROR_COLOR}=${NC}"
done
echo -e ""

printf "${ERROR_COLOR}===== AN ERROR HAS OCCURED ${NC}"
for (( colIndex=27; colIndex<${COLUMNS}; colIndex++ ))
do
    printf "${ERROR_COLOR}=${NC}"
done
echo -e ""

for (( colIndex=0; colIndex<${COLUMNS}; colIndex++ ))
do
    printf "${ERROR_COLOR}=${NC}"
done
echo -e ""
echo -e ""


# Sort out the args given to this script
#echo "Error info: ${@}"
#                       $1              $2        $3         $4            $5            $6...
#                       ${BASH_LINENO}  ${0}      ${LINENO}  ${BASH_ARGC}  ${BASH_ARGV}  ${BASH_COMMAND}
# Example:
# aaa --> Error info:   7               ./aaa.sh  8          1             ./ccc.sh      ls --color=auto --fake-flag
# ccc --> Error info:   0               ./ccc.sh  8                                      ls --color=auto --fake-flag

exitDir=$(pwd)
parent_script_lineno=${1}
parent_script=${2}
faulty_script=${2}
faulty_script_lineno=${3}

if [[ "${parent_script_lineno}" == "0" ]]
then
    # There was no parent script
    shift
    shift
    shift
else
    faulty_script_argc=${4}
    faulty_script=${5}
    shift
    shift
    shift
    shift
    for (( shiftIndex=0; shiftIndex<${faulty_script_argc}; shiftIndex++ ))
    do
        shift
    done
    # FIXME: parent_script_lineno is incorrect if the faulty script is called from a script within a script
    #echo -e "${ERROR_COLOR}From ${parent_script} on line ${parent_script_lineno}${NC}"
fi

#echo "parent_script_lineno: ${parent_script_lineno}"
#echo "parent_script: ${parent_script}"
#echo "faulty_script: ${faulty_script}"
#echo "faulty_script_lineno: ${faulty_script_lineno}"


echo -e "${ERROR_COLOR}An error occured in ${faulty_script} on line ${faulty_script_lineno}${NC}" >&2
echo -e "${ERROR_COLOR}Command that failed: ${@}${NC}" >&2
echo -e ""
echo -e "${WARNING_COLOR}Attempting to fix${NC}"
echo -e ""


# Attempt to fix stuff


# Make sure we have the environment variables set
if [ "${CREATE_SCRIPTS_DIR}" == "" ] && [ -d ~/create_scripts ]
then
    echo "Setting CREATE_SCRIPTS_DIR variable"
    cd ~/create_scripts
    source ./configure_create_scripts.sh
fi

if [ "${SETUP_SCRIPTS_DIR}" == "" ] && [ -d ~/setup_scripts ]
then
    echo "Setting SETUP_SCRIPTS_DIR variable"
    cd ~/setup_scripts
    source ./configure_setup_scripts.sh
fi


# See if the computer is attempting to do daily updates
source ${INSTALL_SCRIPTS_DIR}/tell_me_when_updates_are_done.sh

set +e
#trap - ERR
source ~/.bashrc
set -e
#trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

# Try apt-get stuff
sudo apt-get -y autoremove
sudo apt-get autoclean
sudo apt-get -y check
sudo apt-get update


# Hopefully that fixed things!


# Print a large divider so the user knows we are attempting to run the parent script again
echo -e ""
for (( colIndex=0; colIndex<${COLUMNS}; colIndex++ ))
do
    printf "${WARNING_COLOR}-${NC}"
done
echo -e ""

printf "${WARNING_COLOR}----- Running ${parent_script} again ${NC}"
str_length=${#parent_script}
str_length=$((21+str_length))
for (( colIndex=str_length; colIndex<${COLUMNS}; colIndex++ ))
do
    printf "${WARNING_COLOR}-${NC}"
done
echo -e ""

for (( colIndex=0; colIndex<${COLUMNS}; colIndex++ ))
do
    printf "${WARNING_COLOR}-${NC}"
done
echo -e ""
echo -e ""

cd ${exitDir}
source ${parent_script}
