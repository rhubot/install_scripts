#!/bin/bash -i

# Colors can be found here:  https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
DARK_GRAY='\033[1;30m'
BLACK='\033[0;30m'     
MAROON='\033[0;31m'     
RED='\033[1;31m'
GREEN='\033[0;32m'     
LIME='\033[1;32m'
LIGHT_BROWN='\033[0;33m'     
YELLOW='\033[1;33m'
BLUE='\033[0;34m'     
LIGHT_BLUE='\033[1;34m'
PURPLE='\033[0;35m'     
LIGHT_PURPLE='\033[1;35m'
CYAN='\033[0;36m'     
AQUA='\033[1;36m'
LIGHT_GRAY='\033[0;37m'     
WHITE='\033[1;37m'

NC='\033[0m' # No Color

TITLE_COLOR=${LIGHT_BLUE}
FINISHED_TITLE_COLOR=${BLUE}
PROMPT_COLOR=${LIGHT_BROWN}
WARNING_COLOR=${YELLOW}
ERROR_COLOR=${RED}

# Make sure we are given the proper number of command line arguments
if [ "${1}" == "--demo" ] || [ "${1}" == "--test" ]
then
    echo -e ""
    echo -e "---------- Colors ----------"
    echo -e "${DARK_GRAY}DARK_GRAY!!!${NC}"
    echo -e "${BLACK}BLACK!!!${NC}"
    echo -e "${MAROON}MAROON!!!${NC}"     
    echo -e "${RED}RED!!!${NC}"
    echo -e "${GREEN}GREEN!!!${NC}"     
    echo -e "${LIME}LIME!!!${NC}"
    echo -e "${LIGHT_BROWN}LIGHT_BROWN!!!${NC}"     
    echo -e "${YELLOW}YELLOW!!!${NC}"
    echo -e "${BLUE}BLUE!!!${NC}"     
    echo -e "${LIGHT_BLUE}LIGHT_BLUE!!!${NC}"
    echo -e "${PURPLE}PURPLE!!!${NC}" 
    echo -e "${LIGHT_PURPLE}LIGHT_PURPLE!!!${NC}"
    echo -e "${CYAN}CYAN!!!${NC}"
    echo -e "${AQUA}AQUA!!!${NC}"
    echo -e "${LIGHT_GRAY}LIGHT_GRAY!!!${NC}"  
    echo -e "${WHITE}WHITE!!!${NC}"
    echo -e ""
    echo "---------- Commonly used colors as variables ----------"
    echo -e "${TITLE_COLOR}TITLE_COLOR${NC}"
    echo -e "${FINISHED_TITLE_COLOR}FINISHED_TITLE_COLOR${NC}"
    echo -e "${PROMPT_COLOR}PROMPT_COLOR${NC}"
    echo -e "${WARNING_COLOR}WARNING_COLOR${NC}"
    echo -e "${ERROR_COLOR}ERROR_COLOR${NC}"
    echo -e ""
elif [ "${1}" == "--help" ] || [ "${1}" == "-h" ]
then
    echo -e ""
    echo -e "To use colors within a bash script, simply source the colors.sh file:"
    echo -e "${WARNING_COLOR}    source \${INSTALL_SCRIPTS_DIR}/colors.sh${NC}"
    echo -e "Then use the appropriate variables as needed:"
    echo -e "${WARNING_COLOR}    echo -e \"\${ERROR_COLOR}${ERROR_COLOR}Some colored text${WARNING_COLOR}\${NC}\"${NC}"
    echo -e ""
    echo -e ""
    echo -e "To see the available colors in a demo, use the following command:"
    echo -e "${WARNING_COLOR}    ./colors.sh --demo${NC}"
    echo -e ""
fi
