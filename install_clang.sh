#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# Some variables to make things easier to read
false=0
true=1

# Command line argument variables
notDefaultCompiler=${false}


# A function that displays a help / usage message to the user
function displayHelp() {
    echo -e "Usage:"
    echo -e "    install_clang.sh [options]"
    echo -e ""
    echo -e "Options:"
    echo -e "    --help, -h"
    echo -e "        Show this help message"
    echo -e "    --not-default-compiler, -n"
    echo -e "        Prevents this script from setting CLang as the default compiler"
    
    # Exit out of this script
    exit
}

# Process any command line args
while [ "${1}" != "" ]
do
    case ${1} in
        -h | --help )
            displayHelp
            ;;
        -n | --not-default-compiler )
            notDefaultCompiler=${true}
            ;;
        * )
            echo -e "${WARNING_COLOR}Unknown option provided: ${1}${NC}"
            displayHelp
            ;;
    esac
    shift
done


echo -e ""
echo -e "${TITLE_COLOR}===== Installing CLang =====${NC}"
echo -e ""


# Install CLang
sudo apt-get install -y clang

# Unless otherwise specified, make CLang the default C++ compiler
if [ "${notDefaultCompiler}" == "${false}" ]
then
    sudo update-alternatives --set c++ /usr/bin/clang++
    # NOTE: To manually change this, use the following command:
    # sudo update-alternatives --config c++
fi


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing CLang -----${NC}"
echo -e ""

trap - ERR
