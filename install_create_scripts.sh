#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

# Remember where we started
SCRIPTS_DIR=$(pwd)

# Get color definitions
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing create_scripts Tool =====${NC}"
echo -e ""


sc_user=""
sc_email=""

# Process any command line args
while [ "${1}" != "" ]
do
     case ${1} in
        -u | --sc_user )
            shift
            sc_user=${1}
            ;;
        -e | --sc_email )
            shift
            sc_email=${1}
            ;;
        * )
            echo -e "${WARNING_COLOR}Unknown option provided: ${1}${NC}"
            ;;
    esac
    shift
done

# Make sure we are given the proper command line arguments
if [ "${sc_user}" == "" ] || [ "${sc_email}" == "" ]
then
    # Prompt the user for the info we need
    read -p "$(echo -e ${PROMPT_COLOR}"Username for source control: "${NC})" sc_user
    #read -p "$(echo -e ${PROMPT_COLOR}"Email for source control: "${NC})" sc_email
fi

# Clone the repo
if [ ! -d ~/create_scripts/ ]
then
    git clone https://${sc_user}@bitbucket.org/rhubot/create_scripts.git ~/create_scripts
else
    echo -e "${WARNING_COLOR}~/create_scripts already exists${NC}"
fi


# Set the environment variable so other scripts know where to find the create_scripts directory
if [ "${CREATE_SCRIPTS_DIR}" == "" ]
then
    cd ~/create_scripts
    source ./configure_create_scripts.sh
fi

# Go back to where we started
cd ${SCRIPTS_DIR}


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing create_scripts Tool -----${NC}"
echo -e ""

trap - ERR
