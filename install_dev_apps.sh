#!/bin/bash -i                                                                                                                          
# NOTE: This script was auto generated                                                                                                  
                                                                                                                                        
set -e                                                                                                                                  
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR 
                                                                                                                                        
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""                                                                                                 
                                                                                                                                        
# Specify yes/no values                                                                                                                 
affirm="y"                                                                                                                            
neg="n"                                                                                                                               
unknown="maybe"                                                                                                                       
                                                                                                                                        
sc_user=""                                                                                                                            
sc_email=""                                                                                                                           
                                                                                                                                        
# Set default values                                                                                                                    
choice_0=$affirm                                                                   
choice_1=$neg                                                                      
choice_2=$affirm                                                                   
choice_3=$neg                                                                      
choice_4=$neg                                                                      
choice_5=$affirm                                                                   
choice_6=$affirm                                                                   
choice_7=$neg                                                                      
choice_8=$neg                                                                      
choice_9=$neg                                                                      
choice_10=$neg                                                                      
choice_11=$neg                                                                      
choice_12=$neg                                                                      
choice_13=$neg                                                                      
choice_14=$neg                                                                      
choice_15=$neg                                                                      
choice_16=$neg                                                                      
choice_17=$neg                                                                      
choice_18=$neg                                                                      
choice_19=$neg                                                                      
choice_20=$neg                                                                      
choice_21=$affirm                                                                   
choice_22=$neg                                                                      
choice_23=$neg                                                                      
choice_24=$neg                                                                      
choice_done=$unknown                                                                              
                                                                                                   
# Set index values                                                                                 
index_done=0                                                                                       
index_0=1                                                                    
index_1=2                                                                    
index_2=3                                                                    
index_3=4                                                                    
index_4=5                                                                    
index_5=6                                                                    
index_6=7                                                                    
index_7=8                                                                    
index_8=9                                                                    
index_9=10                                                                    
index_10=11                                                                    
index_11=12                                                                    
index_12=13                                                                    
index_13=14                                                                    
index_14=15                                                                    
index_15=16                                                                    
index_16=17                                                                    
index_17=18                                                                    
index_18=19                                                                    
index_19=20                                                                    
index_20=21                                                                    
index_21=22                                                                    
index_22=23                                                                    
index_23=24                                                                    
index_24=25                                                                    
                                                                                                                                 
# Menu variables                                                                                                                 
currentMenuIndex=1                                                                                                               
user_choice=$affirm                                                                                                             
default_choice=$unkonwn                                                                                                         
                                                                                                                                 
# Display the menu                                                                                                               
fullMenu()                                                                                                                       
{                                                                                                                                
    clear                                                                                                                        
    echo -e "${WARNING_COLOR}NOTE: If you are looking to setup an entire project, you may prefer to use setup_scripts${NC}"  
    echo ""                                                                                                                    
    echo ""                                                                                                                    
    echo "Which apps would you like to install?"                                                                               
    echo ""                                                                                                                    
                                                                                                                                 
    # Git (configure)                                                                                     
    if [ $currentMenuIndex -eq $index_0 ]                                             
    then                                                                                       
        default_choice=$choice_0                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_0"] Git (configure)${NC}"                     
    elif [ "$choice_0" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_0"] Git (configure)${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_0"] Git (configure)${NC}"                           
    fi                                                                                         
                                                                                               
    # Mercurial                                                                                     
    if [ $currentMenuIndex -eq $index_1 ]                                             
    then                                                                                       
        default_choice=$choice_1                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_1"] Mercurial${NC}"                     
    elif [ "$choice_1" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_1"] Mercurial${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_1"] Mercurial${NC}"                           
    fi                                                                                         
                                                                                               
    # Meld                                                                                     
    if [ $currentMenuIndex -eq $index_2 ]                                             
    then                                                                                       
        default_choice=$choice_2                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_2"] Meld${NC}"                     
    elif [ "$choice_2" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_2"] Meld${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_2"] Meld${NC}"                           
    fi                                                                                         
                                                                                               
    # Create Scripts Tool                                                                                     
    if [ $currentMenuIndex -eq $index_3 ]                                             
    then                                                                                       
        default_choice=$choice_3                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_3"] Create Scripts Tool${NC}"                     
    elif [ "$choice_3" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_3"] Create Scripts Tool${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_3"] Create Scripts Tool${NC}"                           
    fi                                                                                         
                                                                                               
    # Setup Scripts Tool                                                                                     
    if [ $currentMenuIndex -eq $index_4 ]                                             
    then                                                                                       
        default_choice=$choice_4                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_4"] Setup Scripts Tool${NC}"                     
    elif [ "$choice_4" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_4"] Setup Scripts Tool${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_4"] Setup Scripts Tool${NC}"                           
    fi                                                                                         
                                                                                               
    # Kate                                                                                     
    if [ $currentMenuIndex -eq $index_5 ]                                             
    then                                                                                       
        default_choice=$choice_5                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_5"] Kate${NC}"                     
    elif [ "$choice_5" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_5"] Kate${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_5"] Kate${NC}"                           
    fi                                                                                         
                                                                                               
    # Tree                                                                                     
    if [ $currentMenuIndex -eq $index_6 ]                                             
    then                                                                                       
        default_choice=$choice_6                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_6"] Tree${NC}"                     
    elif [ "$choice_6" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_6"] Tree${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_6"] Tree${NC}"                           
    fi                                                                                         
                                                                                               
    # WireShark                                                                                     
    if [ $currentMenuIndex -eq $index_7 ]                                             
    then                                                                                       
        default_choice=$choice_7                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_7"] WireShark${NC}"                     
    elif [ "$choice_7" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_7"] WireShark${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_7"] WireShark${NC}"                           
    fi                                                                                         
                                                                                               
    # Docker                                                                                     
    if [ $currentMenuIndex -eq $index_8 ]                                             
    then                                                                                       
        default_choice=$choice_8                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_8"] Docker${NC}"                     
    elif [ "$choice_8" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_8"] Docker${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_8"] Docker${NC}"                           
    fi                                                                                         
                                                                                               
    # Docker Compose                                                                                     
    if [ $currentMenuIndex -eq $index_9 ]                                             
    then                                                                                       
        default_choice=$choice_9                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_9"] Docker Compose${NC}"                     
    elif [ "$choice_9" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_9"] Docker Compose${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_9"] Docker Compose${NC}"                           
    fi                                                                                         
                                                                                               
    # Valgrind                                                                                     
    if [ $currentMenuIndex -eq $index_10 ]                                             
    then                                                                                       
        default_choice=$choice_10                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_10"] Valgrind${NC}"                     
    elif [ "$choice_10" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_10"] Valgrind${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_10"] Valgrind${NC}"                           
    fi                                                                                         
                                                                                               
    # CLang                                                                                     
    if [ $currentMenuIndex -eq $index_11 ]                                             
    then                                                                                       
        default_choice=$choice_11                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_11"] CLang${NC}"                     
    elif [ "$choice_11" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_11"] CLang${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_11"] CLang${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 2.x                                                                                     
    if [ $currentMenuIndex -eq $index_12 ]                                             
    then                                                                                       
        default_choice=$choice_12                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_12"] Python 2.x${NC}"                     
    elif [ "$choice_12" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_12"] Python 2.x${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_12"] Python 2.x${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 2 - Anaconda                                                                                     
    if [ $currentMenuIndex -eq $index_13 ]                                             
    then                                                                                       
        default_choice=$choice_13                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_13"] Python 2 - Anaconda${NC}"                     
    elif [ "$choice_13" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_13"] Python 2 - Anaconda${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_13"] Python 2 - Anaconda${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 2 - Torch                                                                                     
    if [ $currentMenuIndex -eq $index_14 ]                                             
    then                                                                                       
        default_choice=$choice_14                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_14"] Python 2 - Torch${NC}"                     
    elif [ "$choice_14" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_14"] Python 2 - Torch${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_14"] Python 2 - Torch${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 2 - Tensor Flow                                                                                     
    if [ $currentMenuIndex -eq $index_15 ]                                             
    then                                                                                       
        default_choice=$choice_15                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_15"] Python 2 - Tensor Flow${NC}"                     
    elif [ "$choice_15" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_15"] Python 2 - Tensor Flow${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_15"] Python 2 - Tensor Flow${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 3.x                                                                                     
    if [ $currentMenuIndex -eq $index_16 ]                                             
    then                                                                                       
        default_choice=$choice_16                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_16"] Python 3.x${NC}"                     
    elif [ "$choice_16" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_16"] Python 3.x${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_16"] Python 3.x${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 3 - Anaconda                                                                                     
    if [ $currentMenuIndex -eq $index_17 ]                                             
    then                                                                                       
        default_choice=$choice_17                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_17"] Python 3 - Anaconda${NC}"                     
    elif [ "$choice_17" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_17"] Python 3 - Anaconda${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_17"] Python 3 - Anaconda${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 3 - Torch                                                                                     
    if [ $currentMenuIndex -eq $index_18 ]                                             
    then                                                                                       
        default_choice=$choice_18                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_18"] Python 3 - Torch${NC}"                     
    elif [ "$choice_18" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_18"] Python 3 - Torch${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_18"] Python 3 - Torch${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 3 - Tensor Flow                                                                                     
    if [ $currentMenuIndex -eq $index_19 ]                                             
    then                                                                                       
        default_choice=$choice_19                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_19"] Python 3 - Tensor Flow${NC}"                     
    elif [ "$choice_19" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_19"] Python 3 - Tensor Flow${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_19"] Python 3 - Tensor Flow${NC}"                           
    fi                                                                                         
                                                                                               
    # OpenAI Gym                                                                                     
    if [ $currentMenuIndex -eq $index_20 ]                                             
    then                                                                                       
        default_choice=$choice_20                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_20"] OpenAI Gym${NC}"                     
    elif [ "$choice_20" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_20"] OpenAI Gym${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_20"] OpenAI Gym${NC}"                           
    fi                                                                                         
                                                                                               
    # ROS                                                                                     
    if [ $currentMenuIndex -eq $index_21 ]                                             
    then                                                                                       
        default_choice=$choice_21                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_21"] ROS${NC}"                     
    elif [ "$choice_21" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_21"] ROS${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_21"] ROS${NC}"                           
    fi                                                                                         
                                                                                               
    # ROS - MoveIt                                                                                     
    if [ $currentMenuIndex -eq $index_22 ]                                             
    then                                                                                       
        default_choice=$choice_22                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_22"] ROS - MoveIt${NC}"                     
    elif [ "$choice_22" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_22"] ROS - MoveIt${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_22"] ROS - MoveIt${NC}"                           
    fi                                                                                         
                                                                                               
    # Gazebo (with common packages)                                                                                     
    if [ $currentMenuIndex -eq $index_23 ]                                             
    then                                                                                       
        default_choice=$choice_23                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_23"] Gazebo (with common packages)${NC}"                     
    elif [ "$choice_23" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_23"] Gazebo (with common packages)${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_23"] Gazebo (with common packages)${NC}"                           
    fi                                                                                         
                                                                                               
    # Gazebo - GzWeb Client                                                                                     
    if [ $currentMenuIndex -eq $index_24 ]                                             
    then                                                                                       
        default_choice=$choice_24                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_24"] Gazebo - GzWeb Client${NC}"                     
    elif [ "$choice_24" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_24"] Gazebo - GzWeb Client${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_24"] Gazebo - GzWeb Client${NC}"                           
    fi                                                                                         
                                                                                               
    # Done                                                                                         
    if [ $currentMenuIndex -eq $index_done ]                                                     
    then                                                                                           
        default_choice=$affirm                                                                    
        echo -e "${PROMPT_COLOR}--> [y] Done${NC}"                                             
    else                                                                                           
        echo "    [ ] Done"                                                                      
    fi                                                                                             
                                                                                                   
    echo ""                                                                                      
    echo "Menu options:"                                                                         
    echo "  'y' or 'n' to choose if you want to install the currently selected app"              
    echo "  [enter] to accept current value"                                                     
    echo "  'a' to install all listed apps"                                                      
    echo "  'd' to install the currently selected apps"                                          
    echo "  'q' to quit without installing anything"                                             
    echo "  'p' to move to the previously selected app"                                          
                                                                                                   
    # Read the user's choice                                                                       
    read user_choice                                                                               
                                                                                                   
    # Process the user's choice                                                                    
    if [ $user_choice == "a" ]                                                                  
    then                                                                                           
        echo -e "${TITLE_COLOR}Installing all apps${NC}"                                       
        choice_0=$affirm    
        choice_1=$affirm    
        choice_2=$affirm    
        choice_3=$affirm    
        choice_4=$affirm    
        choice_5=$affirm    
        choice_6=$affirm    
        choice_7=$affirm    
        choice_8=$affirm    
        choice_9=$affirm    
        choice_10=$affirm    
        choice_11=$affirm    
        choice_12=$affirm    
        choice_13=$affirm    
        choice_14=$affirm    
        choice_15=$affirm    
        choice_16=$affirm    
        choice_17=$affirm    
        choice_18=$affirm    
        choice_19=$affirm    
        choice_20=$affirm    
        choice_21=$affirm    
        choice_22=$affirm    
        choice_23=$affirm    
        choice_24=$affirm    
        choice_done=$affirm                                                                       
    elif [ $user_choice == "d" ]                                                                
    then                                                                                           
        echo -e "${TITLE_COLOR}Installing currently selected apps${NC}"                        
        choice_done=$affirm                                                                       
    elif [ $user_choice == "q" ]                                                                
    then                                                                                           
        echo -e "${WARNING_COLOR}Exiting without installing anything${NC}"                     
        choice_0=$neg    
        choice_1=$neg    
        choice_2=$neg    
        choice_3=$neg    
        choice_4=$neg    
        choice_5=$neg    
        choice_6=$neg    
        choice_7=$neg    
        choice_8=$neg    
        choice_9=$neg    
        choice_10=$neg    
        choice_11=$neg    
        choice_12=$neg    
        choice_13=$neg    
        choice_14=$neg    
        choice_15=$neg    
        choice_16=$neg    
        choice_17=$neg    
        choice_18=$neg    
        choice_19=$neg    
        choice_20=$neg    
        choice_21=$neg    
        choice_22=$neg    
        choice_23=$neg    
        choice_24=$neg    
        choice_done=$affirm                                                                       
    elif [ $user_choice == "p" ]                                                                
    then                                                                                           
        user_choice=$default_choice                                                               
        decrementMenuIndex                                                                         
    elif [ $user_choice == "y" ]                                                                
    then                                                                                           
        updateChoice                                                                               
    elif [ $user_choice == "n" ]                                                                
    then                                                                                           
        updateChoice                                                                               
    else                                                                                           
        echo "----->"$user_choice"<-----"                                                     
        echo "Using default value"                                                               
        user_choice=$default_choice                                                               
                                                                                                   
        if [ $currentMenuIndex -eq $index_done ]                                                 
        then                                                                                       
            user_choice=$affirm                                                                   
        fi                                                                                         
        updateChoice                                                                               
    fi                                                                                             
}                                                                                                  
                                                                                                   
# Update the choice variables based on user input                                                  
updateChoice()                                                                                     
{                                                                                                  
    # Process user's y/n choice                                                                    
    if [ $currentMenuIndex -eq $index_0 ]                                                 
    then                                                                                           
        choice_0=$user_choice                                                              
        incrementMenuIndex                                                                         
    elif [ $currentMenuIndex -eq $index_1 ]                                           
    then                                                                                       
        choice_1=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_2 ]                                           
    then                                                                                       
        choice_2=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_3 ]                                           
    then                                                                                       
        choice_3=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_4 ]                                           
    then                                                                                       
        choice_4=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_5 ]                                           
    then                                                                                       
        choice_5=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_6 ]                                           
    then                                                                                       
        choice_6=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_7 ]                                           
    then                                                                                       
        choice_7=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_8 ]                                           
    then                                                                                       
        choice_8=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_9 ]                                           
    then                                                                                       
        choice_9=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_10 ]                                           
    then                                                                                       
        choice_10=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_11 ]                                           
    then                                                                                       
        choice_11=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_12 ]                                           
    then                                                                                       
        choice_12=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_13 ]                                           
    then                                                                                       
        choice_13=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_14 ]                                           
    then                                                                                       
        choice_14=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_15 ]                                           
    then                                                                                       
        choice_15=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_16 ]                                           
    then                                                                                       
        choice_16=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_17 ]                                           
    then                                                                                       
        choice_17=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_18 ]                                           
    then                                                                                       
        choice_18=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_19 ]                                           
    then                                                                                       
        choice_19=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_20 ]                                           
    then                                                                                       
        choice_20=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_21 ]                                           
    then                                                                                       
        choice_21=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_22 ]                                           
    then                                                                                       
        choice_22=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_23 ]                                           
    then                                                                                       
        choice_23=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_24 ]                                           
    then                                                                                       
        choice_24=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_done ]                                                   
    then                                                                                           
        choice_done=$user_choice                                                                  
    else                                                                                           
        echo -e "${WARNING_COLOR}Menu index got messed up: ${NC}" $currentMenuIndex           
        choice_done=$affirm                                                                       
    fi                                                                                             
}                                                                                                  
                                                                                                   
# Decrement to the previous menu item                                                              
decrementMenuIndex()                                                                               
{                                                                                                  
    if [ $currentMenuIndex -eq $index_0 ]                                                 
    then                                                                                           
        currentMenuIndex=$index_0                                                          
    elif [ $currentMenuIndex -eq $index_1 ]                                           
    then                                                                                       
        currentMenuIndex=$index_0                                                  
    elif [ $currentMenuIndex -eq $index_2 ]                                           
    then                                                                                       
        currentMenuIndex=$index_1                                                  
    elif [ $currentMenuIndex -eq $index_3 ]                                           
    then                                                                                       
        currentMenuIndex=$index_2                                                  
    elif [ $currentMenuIndex -eq $index_4 ]                                           
    then                                                                                       
        currentMenuIndex=$index_3                                                  
    elif [ $currentMenuIndex -eq $index_5 ]                                           
    then                                                                                       
        currentMenuIndex=$index_4                                                  
    elif [ $currentMenuIndex -eq $index_6 ]                                           
    then                                                                                       
        currentMenuIndex=$index_5                                                  
    elif [ $currentMenuIndex -eq $index_7 ]                                           
    then                                                                                       
        currentMenuIndex=$index_6                                                  
    elif [ $currentMenuIndex -eq $index_8 ]                                           
    then                                                                                       
        currentMenuIndex=$index_7                                                  
    elif [ $currentMenuIndex -eq $index_9 ]                                           
    then                                                                                       
        currentMenuIndex=$index_8                                                  
    elif [ $currentMenuIndex -eq $index_10 ]                                           
    then                                                                                       
        currentMenuIndex=$index_9                                                  
    elif [ $currentMenuIndex -eq $index_11 ]                                           
    then                                                                                       
        currentMenuIndex=$index_10                                                  
    elif [ $currentMenuIndex -eq $index_12 ]                                           
    then                                                                                       
        currentMenuIndex=$index_11                                                  
    elif [ $currentMenuIndex -eq $index_13 ]                                           
    then                                                                                       
        currentMenuIndex=$index_12                                                  
    elif [ $currentMenuIndex -eq $index_14 ]                                           
    then                                                                                       
        currentMenuIndex=$index_13                                                  
    elif [ $currentMenuIndex -eq $index_15 ]                                           
    then                                                                                       
        currentMenuIndex=$index_14                                                  
    elif [ $currentMenuIndex -eq $index_16 ]                                           
    then                                                                                       
        currentMenuIndex=$index_15                                                  
    elif [ $currentMenuIndex -eq $index_17 ]                                           
    then                                                                                       
        currentMenuIndex=$index_16                                                  
    elif [ $currentMenuIndex -eq $index_18 ]                                           
    then                                                                                       
        currentMenuIndex=$index_17                                                  
    elif [ $currentMenuIndex -eq $index_19 ]                                           
    then                                                                                       
        currentMenuIndex=$index_18                                                  
    elif [ $currentMenuIndex -eq $index_20 ]                                           
    then                                                                                       
        currentMenuIndex=$index_19                                                  
    elif [ $currentMenuIndex -eq $index_21 ]                                           
    then                                                                                       
        currentMenuIndex=$index_20                                                  
    elif [ $currentMenuIndex -eq $index_22 ]                                           
    then                                                                                       
        currentMenuIndex=$index_21                                                  
    elif [ $currentMenuIndex -eq $index_23 ]                                           
    then                                                                                       
        currentMenuIndex=$index_22                                                  
    elif [ $currentMenuIndex -eq $index_24 ]                                           
    then                                                                                       
        currentMenuIndex=$index_23                                                  
    elif [ $currentMenuIndex -eq $index_done ]                                                   
    then                                                                                           
        currentMenuIndex=$index_24                                                      
    else                                                                                           
        currentMenuIndex=$index_done                                                              
    fi                                                                                             
}                                                                                                  
                                                                                                   
# Increment to the next menu item                                                                  
incrementMenuIndex()                                                                               
{                                                                                                  
    if [ $currentMenuIndex -eq $index_0 ]                                                 
    then                                                                                           
        currentMenuIndex=$index_1                                                      
    elif [ $currentMenuIndex -eq $index_1 ]                                           
    then                                                                                       
        currentMenuIndex=$index_2                                                  
    elif [ $currentMenuIndex -eq $index_2 ]                                           
    then                                                                                       
        currentMenuIndex=$index_3                                                  
    elif [ $currentMenuIndex -eq $index_3 ]                                           
    then                                                                                       
        currentMenuIndex=$index_4                                                  
    elif [ $currentMenuIndex -eq $index_4 ]                                           
    then                                                                                       
        currentMenuIndex=$index_5                                                  
    elif [ $currentMenuIndex -eq $index_5 ]                                           
    then                                                                                       
        currentMenuIndex=$index_6                                                  
    elif [ $currentMenuIndex -eq $index_6 ]                                           
    then                                                                                       
        currentMenuIndex=$index_7                                                  
    elif [ $currentMenuIndex -eq $index_7 ]                                           
    then                                                                                       
        currentMenuIndex=$index_8                                                  
    elif [ $currentMenuIndex -eq $index_8 ]                                           
    then                                                                                       
        currentMenuIndex=$index_9                                                  
    elif [ $currentMenuIndex -eq $index_9 ]                                           
    then                                                                                       
        currentMenuIndex=$index_10                                                  
    elif [ $currentMenuIndex -eq $index_10 ]                                           
    then                                                                                       
        currentMenuIndex=$index_11                                                  
    elif [ $currentMenuIndex -eq $index_11 ]                                           
    then                                                                                       
        currentMenuIndex=$index_12                                                  
    elif [ $currentMenuIndex -eq $index_12 ]                                           
    then                                                                                       
        currentMenuIndex=$index_13                                                  
    elif [ $currentMenuIndex -eq $index_13 ]                                           
    then                                                                                       
        currentMenuIndex=$index_14                                                  
    elif [ $currentMenuIndex -eq $index_14 ]                                           
    then                                                                                       
        currentMenuIndex=$index_15                                                  
    elif [ $currentMenuIndex -eq $index_15 ]                                           
    then                                                                                       
        currentMenuIndex=$index_16                                                  
    elif [ $currentMenuIndex -eq $index_16 ]                                           
    then                                                                                       
        currentMenuIndex=$index_17                                                  
    elif [ $currentMenuIndex -eq $index_17 ]                                           
    then                                                                                       
        currentMenuIndex=$index_18                                                  
    elif [ $currentMenuIndex -eq $index_18 ]                                           
    then                                                                                       
        currentMenuIndex=$index_19                                                  
    elif [ $currentMenuIndex -eq $index_19 ]                                           
    then                                                                                       
        currentMenuIndex=$index_20                                                  
    elif [ $currentMenuIndex -eq $index_20 ]                                           
    then                                                                                       
        currentMenuIndex=$index_21                                                  
    elif [ $currentMenuIndex -eq $index_21 ]                                           
    then                                                                                       
        currentMenuIndex=$index_22                                                  
    elif [ $currentMenuIndex -eq $index_22 ]                                           
    then                                                                                       
        currentMenuIndex=$index_23                                                  
    elif [ $currentMenuIndex -eq $index_23 ]                                           
    then                                                                                       
        currentMenuIndex=$index_24                                                  
    elif [ $currentMenuIndex -eq $index_24 ]                                               
    then                                                                                           
        currentMenuIndex=$index_done                                                              
    else                                                                                           
        currentMenuIndex=$index_done                                                              
    fi                                                                                             
}                                                                                                  
                                                                                                   
# Loop thorugh the menu until done                                                                 
loop()                                                                                             
{                                                                                                  
    fullMenu                                                                                       
                                                                                                   
    if [ $choice_done == $affirm ]                                                               
    then                                                                                           
        echo -e "${FINISHED_TITLE_COLOR}Done${NC}"                                             
    elif [ $choice_done == $neg ]                                                                
    then                                                                                           
        currentMenuIndex=1                                                                         
        choice_done=$unknown                                                                      
        loop                                                                                       
    else                                                                                           
        loop                                                                                       
    fi                                                                                             
}                                                                                                  
                                                                                                  
# When installing source control, we will need more info                                          
getSourceControlParams()                                                                          
{                                                                                                 
    read -p "$(echo -e ${PROMPT_COLOR}"Username for source control: "${NC})" sc_user       
    read -p "$(echo -e ${PROMPT_COLOR}"Email for source control: "${NC})" sc_email         
}                                                                                                 
                                                                                                  
                                                                                                   
                                                                                                   
# Start the menu loop                                                                              
loop                                                                                               
                                                                                                   
# Install stuff                                                                                    
if [ $user_choice != "q" ]                                                                      
then                                                                                               
    clear                                                                                          
    getSourceControlParams                                                                         
    clear                                                                                          
    echo -e ""                                                                                   
    echo -e "${TITLE_COLOR}~~~~~ Installing apps ~~~~~${NC}"                                   
    echo -e ""                                                                                   
    sudo apt-get update                                                                            
                                                                                                   
                                                                                                   
    # Install the selected apps                                                                    
                                                                                               
if [ "${choice_0,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_git.sh --sc_user ${sc_user} --sc_email ${sc_email}                            
fi                                                                                             
                                                                                               
if [ "${choice_1,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_mercurial.sh --sc_user ${sc_user} --sc_email ${sc_email}                            
fi                                                                                             
                                                                                               
if [ "${choice_2,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_meld.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_3,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_create_scripts.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_4,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_setup_project_scripts.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_5,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_kate.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_6,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_tree.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_7,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_wireshark.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_8,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_docker.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_9,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_dockerCompose.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_10,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_valgrind.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_11,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_clang.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_12,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_core.sh 2                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_13,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_anaconda.sh 2                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_14,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_pytorch.sh 2                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_15,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_tensorflow.sh 2                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_16,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_core.sh 3                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_17,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_anaconda.sh 3                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_18,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_pytorch.sh 3                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_19,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_tensorflow.sh 3                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_20,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_openai_gym.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_21,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_ros.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_22,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_ros_moveit.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_23,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_gazebo.sh                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_24,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_gazebo_gzweb.sh                                                                          
fi                                                                                             
                                                                                                   
    echo -e ""                                                                                   
    echo -e "${FINISHED_TITLE_COLOR}***** Finished installing apps *****${NC}"                 
    echo -e ""                                                                                   
fi                                                                                                 
                                                                                                   
trap - ERR                                                                                         
