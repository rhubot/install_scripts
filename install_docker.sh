#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Docker =====${NC}"
echo -e ""

########################
# Setup the Repository #
########################

# The following commands came from:
#    https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository

# Remove old Docker installs
sudo apt-get remove -y docker docker-engine docker.io

# Install packages to allow apt to use a repository over HTTPS:
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Set up the stable repository
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

#####################
# Install Docker CE #
#####################

# Update the apt package index
sudo apt-get update

# Install the latest version of Docker CE
sudo apt-get install -y docker-ce

####################################
# Manage Docker as a non-root user #
####################################

# Create the docker group
docker_group="docker"
if ! grep -q ${docker_group} /etc/group
then
    sudo groupadd ${docker_group}
fi

# Add your user to the docker group if they aren't already part of the group
if ! groups ${USER} | grep &>/dev/null "\b${docker_group}\b"
then
    sudo usermod -aG docker $USER
fi


echo -e ""
for (( colIndex=0; colIndex<${COLUMNS}; colIndex++ ))
do
    printf "${WARNING_COLOR}&${NC}"
done
echo -e ""

printf "${WARNING_COLOR}&&&&& REMINDER ${NC}"
for (( colIndex=15; colIndex<${COLUMNS}; colIndex++ ))
do
    printf "${WARNING_COLOR}&${NC}"
done
echo -e ""

for (( colIndex=0; colIndex<${COLUMNS}; colIndex++ ))
do
    printf "${WARNING_COLOR}&${NC}"
done
echo -e ""
echo -e ""
echo -e "${WARNING_COLOR}You probably need to log-out / log-in so group membership is re-evaluated${NC}"
echo -e ""


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing Docker -----${NC}"
echo -e ""

trap - ERR
