#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Gazebo =====${NC}"
echo -e ""


VER=$(lsb_release -sr)

if [ $VER == "14.04" ] || [ $VER == "13.10" ]
then
    # For ROS Indigo
    sudo apt-get install -y gazebo2 ros-Indigo-gazebo-ros-pkgs ros-indigo-gazebo-ros-control
elif [ $VER == "16.04" ] || [ $VER == "15.10" ]
then
    # For ROS Kinetic
    sudo apt-get install -y gazebo7 ros-kinetic-gazebo-ros-pkgs ros-kinetic-gazebo-ros-control
    # For advanced control of Gazebo...
    sudo apt-get install -y ros-kinetic-rqt-common-plugins ros-kinetic-dynamic-reconfigure
    #ros-kinetic-ros-control ros-kinetic-ros-controllers
elif [ $VER == "18.04" ] || [ $VER == "17.10" ]
then
    # For ROS Melodic
    sudo apt-get install -y gazebo9
else
    echo -e "${ERROR_COLOR}Script not yet implemented for this version of Ubuntu/Gazebo${NC}"
fi

echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing Gazebo -----${NC}"
echo -e ""

trap - ERR
