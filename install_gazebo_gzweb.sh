#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing GzWeb =====${NC}"
echo -e ""


VER=$(lsb_release -sr)

source ./colors.sh

if [ $VER == "14.04" ] || [ $VER == "13.10" ]
then
    ## For ROS Indigo
    echo -e "${ERROR_COLOR}Error: GzWeb is not available for this version of Ubuntu/ROS${NC}"
elif [ $VER == "16.04" ] || [ $VER == "15.10" ]
then
    ## For ROS Kinetic
    sudo apt-get install -y libgazebo7-dev
    sudo apt install -y libjansson-dev nodejs npm nodejs-legacy libboost-dev imagemagick libtinyxml-dev mercurial cmake build-essential
    
    cd ~
    hg clone https://bitbucket.org/osrf/gzweb
    
    cd ~/gzweb
    hg up gzweb_1.4.0
    
    source /usr/share/gazebo/setup.sh
    
    npm run deploy --- -m
    
elif [ $VER == "18.04" ] || [ $VER == "17.10" ]
then
    ## For ROS Melodic
    echo -e "${ERROR_COLOR}This will fail...${NC}"
    
    # Gather info from user
    read -p "Username for source control: " sc_user
    read -p "Email for source control: " sc_email
    source ./install_mercurial.sh $sc_user $sc_email
    
    # Install dependencies
    sudo apt-get install -y libgazebo9-dev
    sudo apt-get install -y libssl1.0-dev nodejs-dev node-gyp npm

    #sudo apt install libjansson-dev nodejs npm nodejs-legacy libboost-dev imagemagick libtinyxml-dev mercurial cmake build-essential
    #sudo apt install -y libjansson-dev nodejs npm nodejs:i386 libboost-dev imagemagick libtinyxml-dev mercurial cmake build-essential
    sudo apt install -y libjansson-dev npm libboost-dev imagemagick libtinyxml-dev mercurial cmake build-essential
    
    cd ~
    hg clone https://bitbucket.org/osrf/gzweb
    
    cd ~/gzweb
    hg up gzweb_1.4.0
    
    source /usr/share/gazebo/setup.sh
    
    npm run deploy --- -m -c
    
else
    echo -e "${ERROR_COLOR}Script not yet implemented for this version of Ubuntu/Gazebo${NC}"
fi

echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing GzWeb -----${NC}"
echo -e ""

trap - ERR
