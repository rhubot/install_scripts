#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

# Remember where we started
SCRIPTS_DIR=$(pwd)

# Get color definitions
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Git =====${NC}"
echo -e ""


sc_user=""
sc_email=""

# Process any command line args
while [ "${1}" != "" ]
do
     case ${1} in
        -u | --sc_user )
            shift
            sc_user=${1}
            ;;
        -e | --sc_email )
            shift
            sc_email=${1}
            ;;
        * )
            echo -e "${WARNING_COLOR}Unknown option provided: ${1}${NC}"
            ;;
    esac
    shift
done

# Make sure we are given the proper command line arguments
if [ "${sc_user}" == "" ] || [ "${sc_email}" == "" ]
then
    # Prompt the user for the info we need
    read -p "$(echo -e ${PROMPT_COLOR}"Username for source control: "${NC})" sc_user
    read -p "$(echo -e ${PROMPT_COLOR}"Email for source control: "${NC})" sc_email
fi

# Get GIT
sudo apt-get install -y git

# TODO: Install GIT LFS
# https://github.com/git-lfs/git-lfs/wiki/Installation

# Configure GIT
git config --global user.email ${sc_email}
git config --global user.name ${sc_user}

git config --global push.default simple
git config --global credential.helper 'cache --timeout=7200'

# Return from where we came
cd ${SCRIPTS_DIR}


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing Git -----${NC}"
echo -e ""

trap - ERR
