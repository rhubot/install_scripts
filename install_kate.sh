#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Kate =====${NC}"
echo -e ""


sudo apt-get install -y kate

# Setup the config files
CONFIG_FILE_KATERC=~/.config/katerc
CONFIG_FILE_KATEPARTRC=~/.config/katepartrc
CONFIG_FILE_KATEVIRC=~/.config/katevirc
CONFIG_FILE_KATESYNTAXHIGHLIGHTINRC=~/.config/katesyntaxhighlightingrc
CONFIG_FILE_KATESCHEMARC=~/.config/kateschemarc

touch ${CONFIG_FILE_KATERC}
echo "[General]                                                                                                                         " >> ${CONFIG_FILE_KATERC}
echo "Config Revision=10                                                                                                                " >> ${CONFIG_FILE_KATERC}
echo "Days Meta Infos=30                                                                                                                " >> ${CONFIG_FILE_KATERC}
echo "Last Session=                                                                                                                     " >> ${CONFIG_FILE_KATERC}
echo "Modified Notification=false                                                                                                       " >> ${CONFIG_FILE_KATERC}
echo "Recent File List Entry Count=10                                                                                                   " >> ${CONFIG_FILE_KATERC}
echo "Restore Window Configuration=true                                                                                                 " >> ${CONFIG_FILE_KATERC}
echo "Save Meta Infos=true                                                                                                              " >> ${CONFIG_FILE_KATERC}
echo "Show Full Path in Title=false                                                                                                     " >> ${CONFIG_FILE_KATERC}
echo "Show Menu Bar=true                                                                                                                " >> ${CONFIG_FILE_KATERC}
echo "Show Status Bar=true                                                                                                              " >> ${CONFIG_FILE_KATERC}
echo "Show Tab Bar=true                                                                                                                 " >> ${CONFIG_FILE_KATERC}
echo "Startup Session=last                                                                                                              " >> ${CONFIG_FILE_KATERC}
echo "                                                                                                                                  " >> ${CONFIG_FILE_KATERC}
echo "[MainWindow]                                                                                                                      " >> ${CONFIG_FILE_KATERC}
echo "Height 2005=1953                                                                                                                  " >> ${CONFIG_FILE_KATERC}
echo "State=AAAA/wAAAAD9AAAAAAAAB2AAAAeIAAAABAAAAAQAAAAIAAAACPwAAAABAAAAAgAAAAEAAAAWAG0AYQBpAG4AVABvAG8AbABCAGEAcgAAAAAA/////wAAAAAAAAAA" >> ${CONFIG_FILE_KATERC}
echo "ToolBarsMovable=Disabled                                                                                                          " >> ${CONFIG_FILE_KATERC}
echo "Width 3840=1888                                                                                                                   " >> ${CONFIG_FILE_KATERC}
echo "Window-Maximized 480x640=true                                                                                                     " >> ${CONFIG_FILE_KATERC}
echo "                                                                                                                                  " >> ${CONFIG_FILE_KATERC}
echo "[filetree]                                                                                                                        " >> ${CONFIG_FILE_KATERC}
echo "editShade=179,218,246                                                                                                             " >> ${CONFIG_FILE_KATERC}
echo "listMode=false                                                                                                                    " >> ${CONFIG_FILE_KATERC}
echo "shadingEnabled=true                                                                                                               " >> ${CONFIG_FILE_KATERC}
echo "showFullPathOnRoots=false                                                                                                         " >> ${CONFIG_FILE_KATERC}
echo "sortRole=0                                                                                                                        " >> ${CONFIG_FILE_KATERC}
echo "Shade=193,208,209                                                                                                                 " >> ${CONFIG_FILE_KATERC}

touch ${CONFIG_FILE_KATEPARTRC}
echo "[Document]                           " >> ${CONFIG_FILE_KATEPARTRC}
echo "Allow End of Line Detection=true     " >> ${CONFIG_FILE_KATEPARTRC}
echo "BOM=false                            " >> ${CONFIG_FILE_KATEPARTRC}
echo "Backup Flags=0                       " >> ${CONFIG_FILE_KATEPARTRC}
echo "Backup Prefix=                       " >> ${CONFIG_FILE_KATEPARTRC}
echo "Backup Suffix=~                      " >> ${CONFIG_FILE_KATEPARTRC}
echo "Encoding=UTF-8                       " >> ${CONFIG_FILE_KATEPARTRC}
echo "End of Line=0                        " >> ${CONFIG_FILE_KATEPARTRC}
echo "Indent On Backspace=true             " >> ${CONFIG_FILE_KATEPARTRC}
echo "Indent On Tab=true                   " >> ${CONFIG_FILE_KATEPARTRC}
echo "Indent On Text Paste=false           " >> ${CONFIG_FILE_KATEPARTRC}
echo "Indentation Mode=normal              " >> ${CONFIG_FILE_KATEPARTRC}
echo "Indentation Width=4                  " >> ${CONFIG_FILE_KATEPARTRC}
echo "Keep Extra Spaces=false              " >> ${CONFIG_FILE_KATEPARTRC}
echo "Line Length Limit=4096               " >> ${CONFIG_FILE_KATEPARTRC}
echo "Newline At EOF=false                 " >> ${CONFIG_FILE_KATEPARTRC}
echo "On-The-Fly Spellcheck=false          " >> ${CONFIG_FILE_KATEPARTRC}
echo "Overwrite Mode=false                 " >> ${CONFIG_FILE_KATEPARTRC}
echo "PageUp/PageDown Moves Cursor=false   " >> ${CONFIG_FILE_KATEPARTRC}
echo "Remove Spaces=0                      " >> ${CONFIG_FILE_KATEPARTRC}
echo "ReplaceTabsDyn=true                  " >> ${CONFIG_FILE_KATEPARTRC}
echo "Show Spaces=false                    " >> ${CONFIG_FILE_KATEPARTRC}
echo "Show Tabs=true                       " >> ${CONFIG_FILE_KATEPARTRC}
echo "Smart Home=true                      " >> ${CONFIG_FILE_KATEPARTRC}
echo "Swap Directory=                      " >> ${CONFIG_FILE_KATEPARTRC}
echo "Swap File Mode=1                     " >> ${CONFIG_FILE_KATEPARTRC}
echo "Swap Sync Interval=15                " >> ${CONFIG_FILE_KATEPARTRC}
echo "Tab Handling=2                       " >> ${CONFIG_FILE_KATEPARTRC}
echo "Tab Width=8                          " >> ${CONFIG_FILE_KATEPARTRC}
echo "Word Wrap=false                      " >> ${CONFIG_FILE_KATEPARTRC}
echo "Word Wrap Column=80                  " >> ${CONFIG_FILE_KATEPARTRC}
echo "                                     " >> ${CONFIG_FILE_KATEPARTRC}
echo "[Editor]                             " >> ${CONFIG_FILE_KATEPARTRC}
echo "Encoding Prober Type=1               " >> ${CONFIG_FILE_KATEPARTRC}
echo "Fallback Encoding=ISO-8859-15        " >> ${CONFIG_FILE_KATEPARTRC}
echo "                                     " >> ${CONFIG_FILE_KATEPARTRC}
echo "[Renderer]                           " >> ${CONFIG_FILE_KATEPARTRC}
echo "Animate Bracket Matching=false       " >> ${CONFIG_FILE_KATEPARTRC}
echo "Schema=Normal                        " >> ${CONFIG_FILE_KATEPARTRC}
echo "Show Indentation Lines=false         " >> ${CONFIG_FILE_KATEPARTRC}
echo "Show Whole Bracket Expression=false  " >> ${CONFIG_FILE_KATEPARTRC}
echo "Word Wrap Marker=false               " >> ${CONFIG_FILE_KATEPARTRC}
echo "                                     " >> ${CONFIG_FILE_KATEPARTRC}
echo "[View]                               " >> ${CONFIG_FILE_KATEPARTRC}
echo "Allow Mark Menu=true                 " >> ${CONFIG_FILE_KATEPARTRC}
echo "Auto Brackets=false                  " >> ${CONFIG_FILE_KATEPARTRC}
echo "Auto Center Lines=0                  " >> ${CONFIG_FILE_KATEPARTRC}
echo "Auto Completion=true                 " >> ${CONFIG_FILE_KATEPARTRC}
echo "Bookmark Menu Sorting=0              " >> ${CONFIG_FILE_KATEPARTRC}
echo "Default Mark Type=1                  " >> ${CONFIG_FILE_KATEPARTRC}
echo "Dynamic Word Wrap=false              " >> ${CONFIG_FILE_KATEPARTRC}
echo "Dynamic Word Wrap Align Indent=80    " >> ${CONFIG_FILE_KATEPARTRC}
echo "Dynamic Word Wrap Indicators=1       " >> ${CONFIG_FILE_KATEPARTRC}
echo "Fold First Line=false                " >> ${CONFIG_FILE_KATEPARTRC}
echo "Folding Bar=true                     " >> ${CONFIG_FILE_KATEPARTRC}
echo "Icon Bar=false                       " >> ${CONFIG_FILE_KATEPARTRC}
echo "Input Mode=0                         " >> ${CONFIG_FILE_KATEPARTRC}
echo "Keyword Completion=true              " >> ${CONFIG_FILE_KATEPARTRC}
echo "Line Modification=true               " >> ${CONFIG_FILE_KATEPARTRC}
echo "Line Numbers=true                    " >> ${CONFIG_FILE_KATEPARTRC}
echo "Maximum Search History Size=100      " >> ${CONFIG_FILE_KATEPARTRC}
echo "Persistent Selection=false           " >> ${CONFIG_FILE_KATEPARTRC}
echo "Scroll Bar Marks=false               " >> ${CONFIG_FILE_KATEPARTRC}
echo "Scroll Bar Mini Map=true             " >> ${CONFIG_FILE_KATEPARTRC}
echo "Scroll Bar Mini Map All=false        " >> ${CONFIG_FILE_KATEPARTRC}
echo "Scroll Bar Mini Map Width=60         " >> ${CONFIG_FILE_KATEPARTRC}
echo "Scroll Past End=false                " >> ${CONFIG_FILE_KATEPARTRC}
echo "Search/Replace Flags=140             " >> ${CONFIG_FILE_KATEPARTRC}
echo "Show Scrollbars=0                    " >> ${CONFIG_FILE_KATEPARTRC}
echo "Show Word Count=false                " >> ${CONFIG_FILE_KATEPARTRC}
echo "Smart Copy Cut=false                 " >> ${CONFIG_FILE_KATEPARTRC}
echo "Vi Input Mode Steal Keys=false       " >> ${CONFIG_FILE_KATEPARTRC}
echo "Vi Relative Line Numbers=false       " >> ${CONFIG_FILE_KATEPARTRC}
echo "Word Completion=true                 " >> ${CONFIG_FILE_KATEPARTRC}
echo "Word Completion Minimal Word Length=3" >> ${CONFIG_FILE_KATEPARTRC}
echo "Word Completion Remove Tail=false    " >> ${CONFIG_FILE_KATEPARTRC}

touch ${CONFIG_FILE_KATEVIRC}
echo "[Kate Vi Input Mode Settings]   " >> ${CONFIG_FILE_KATEVIRC}
echo "Command Mode Mapping Keys=      " >> ${CONFIG_FILE_KATEVIRC}
echo "Command Mode Mappings=          " >> ${CONFIG_FILE_KATEVIRC}
echo "Command Mode Mappings Recursion=" >> ${CONFIG_FILE_KATEVIRC}
echo "Insert Mode Mapping Keys=       " >> ${CONFIG_FILE_KATEVIRC}
echo "Insert Mode Mappings=           " >> ${CONFIG_FILE_KATEVIRC}
echo "Insert Mode Mappings Recursion= " >> ${CONFIG_FILE_KATEVIRC}
echo "Macro Completions=              " >> ${CONFIG_FILE_KATEVIRC}
echo "Macro Contents=                 " >> ${CONFIG_FILE_KATEVIRC}
echo "Macro Registers=                " >> ${CONFIG_FILE_KATEVIRC}
echo "Map Leader=\\                   " >> ${CONFIG_FILE_KATEVIRC}
echo "Normal Mode Mapping Keys=       " >> ${CONFIG_FILE_KATEVIRC}
echo "Normal Mode Mappings=           " >> ${CONFIG_FILE_KATEVIRC}
echo "Normal Mode Mappings Recursion= " >> ${CONFIG_FILE_KATEVIRC}
echo "Visual Mode Mapping Keys=       " >> ${CONFIG_FILE_KATEVIRC}
echo "Visual Mode Mappings=           " >> ${CONFIG_FILE_KATEVIRC}
echo "Visual Mode Mappings Recursion= " >> ${CONFIG_FILE_KATEVIRC}

touch ${CONFIG_FILE_KATESYNTAXHIGHLIGHTINRC}
echo "[Highlighting None - Schema Normal]" >> ${CONFIG_FILE_KATESYNTAXHIGHLIGHTINRC}
echo "Normal Text=0,,,,,,,,,,---         " >> ${CONFIG_FILE_KATESYNTAXHIGHLIGHTINRC}

touch ${CONFIG_FILE_KATESCHEMARC}
echo "[Normal]                           " >> ${CONFIG_FILE_KATESCHEMARC}
echo "Font=monospace,11,-1,2,50,0,0,0,0,0" >> ${CONFIG_FILE_KATESCHEMARC}
echo "dummy=prevent-empty-group          " >> ${CONFIG_FILE_KATESCHEMARC}


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing Kate -----${NC}"
echo -e ""

trap - ERR
