#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Meld =====${NC}"
echo -e ""

sudo apt-get install -y meld

sudo echo "#!/bin/bash" >> ~/git-meld
sudo echo 'meld $2 $5' >> ~/git-meld

sudo chmod 755 ~/git-meld

sudo mv ~/git-meld /bin/

echo '[diff] external = git-meld' >> ~/.gitconfig

echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing Meld -----${NC}"
echo -e ""

trap - ERR
