#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

# Remember where we started
SCRIPTS_DIR=$(pwd)

# Get color definitions
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Mercurial =====${NC}"
echo -e ""


sc_user=""
sc_email=""

# Process any command line args
while [ "${1}" != "" ]
do
     case ${1} in
        -u | --sc_user )
            shift
            sc_user=${1}
            ;;
        -e | --sc_email )
            shift
            sc_email=${1}
            ;;
        * )
            echo -e "${WARNING_COLOR}Unknown option provided: ${1}${NC}"
            ;;
    esac
    shift
done

# Make sure we are given the proper command line arguments
if [ "${sc_user}" == "" ] || [ "${sc_email}" == "" ]
then
    # Prompt the user for the info we need
    read -p "$(echo -e ${PROMPT_COLOR}"Username for source control: "${NC})" sc_user
    read -p "$(echo -e ${PROMPT_COLOR}"Email for source control: "${NC})" sc_email
fi

# Get Mercurial
sudo apt-get install -y mercurial

# Configure Mercurial
echo "[UI]" >> ~/.hgrc
echo "# Name data to appear in commits" >> ~/.hgrc
echo "username = ${sc_user} <${sc_email}>" >> ~/.hgrc

# Return from where we came
cd ${SCRIPTS_DIR}


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing Mercurial -----${NC}"
echo -e ""

trap - ERR
