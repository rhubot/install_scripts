#!/bin/bash -i

# verify that Nvidia hardware is present:
VIDEO_CARD=$(lspci | grep -i nvidia)
#VIDEO_CARD=$(lspci | grep -i bob)

if [ -z "${VIDEO_CARD}" ]
then
    echo "You do not have an nVidia video card"
    exit 1
else
    echo "You have a nVidia video card"
fi

# install GCC
sudo apt-get install -y build-essential

# install Nvidia kernel headers and development packages for the currently running kernel
sudo apt-get install -y linux-headers-$(uname -r)


wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
sudo add-apt-repository "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/ /"
sudo apt-get update
sudo apt-get -y install cuda


echo "export PATH=/usr/local/cuda-10.2/bin:/usr/local/cuda-10.2/NsightCompute-2019.1${PATH:+:${PATH}}" >> ~/.bashrc
set +e
source ~/.bashrc

# Add Docker support, if Docker is installed
DOCKER_VER=$(docker --version)

if [[ "${DOCKER_VER}" == *"build"* ]]
then
    echo "Adding NVidia docker support"
    
    # Add the package repositories
    distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
    curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
    curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
    
    # install the nvidia docker support packages
    sudo apt-get update 
    sudo apt-get install -y nvidia-container-toolkit
    
    # restart the docker daemon with nvidia support
    sudo systemctl restart docker
fi
