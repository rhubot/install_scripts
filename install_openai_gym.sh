#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

# Make sure we are given the proper number of command line arguments
if test $# -ne 1
then
    echo -e "${ERROR_COLOR}Usage: ${0} [2|3]${NC}"
    echo -e "${ERROR_COLOR}Given $# command line arguments${NC}"
    exit 1
fi

echo -e ""
echo -e "${TITLE_COLOR}===== Installing OpenAI Gym =====${NC}"
echo -e ""

# Get the command line arguments
py_ver=${1}

# Install Gym
if [ ${py_ver} == "2" ]
then

    # Remember where we started
    SCRIPTS_DIR=$(pwd)

    # Make sure Python and pip are installed
    source ${INSTALL_SCRIPTS_DIR}/install_python_core.sh ${py_ver}
    set +e
    source ~/.bashrc
    set -e
    
    if [ ! -d ~/gym/ ]
    then
        git clone https://github.com/openai/gym.git ~/gym
        
        # OpenAI Gym does not specify a version of scipy and the latest version does not support python 2.x
        # Specify a version of scipy that works with Python 2.x
        sed -i 's/scipy/scipy<=1.1.0/g' ~/gym/setup.py
    else
        echo -e "${WARNING_COLOR}~/gym already exists${NC}"
    fi
    
    cd ~/gym
    pip install --default-timeout=100 --user -e .
    
    # Get the OS version so we can determine which dependencies to install
    VER=$(lsb_release -sr)
    
    # Install dependencies
    sudo apt-get update
    if [ $VER == "16.04" ] || [ $VER == "15.10" ]
    then
        sudo apt-get install -y build-essential cmake python-dev python-opengl libboost-all-dev zlib1g-dev libsdl2-dev libav-tools xorg-dev libjpeg-dev swig
    elif [ $VER == "18.04" ] || [ $VER == "17.10" ]
    then
        sudo apt-get install -y build-essential cmake python-dev python-opengl libboost-all-dev zlib1g-dev libsdl2-dev ffmpeg:i386 xorg-dev libjpeg-dev swig
    else
        echo -e "${WARNING_COLOR}Script not yet implemented for this version of Ubuntu/ROS${NC}"
        echo -e "Attempting to install dependencies anyway..."
        
        sudo apt-get install -y build-essential cmake python-dev python-opengl libboost-all-dev zlib1g-dev libsdl2-dev libav-tools xorg-dev libjpeg-dev swig
    fi
    

    #pip install --default-timeout=100 --user gym

    #pip install --default-timeout=100 --user --ignore-installed pip

    #sudo apt-get remove -y python-numpy

    #pip install --default-timeout=100 --user 'gym[atari]'
    #pip install --default-timeout=100 --user 'gym[box2d]'
    #pip install --default-timeout=100 --user 'gym[classic_control]'
    
    #pip install --default-timeout=100 --user -e '.[atari]'
    pip install --default-timeout=100 --user -e '.[box2d]'
    #conda install -y -c https://conda.anaconda.org/kne pybox2d

    pip install --default-timeout=100 --user -e '.[classic_control]'
    #pip install --default-timeout=100 -e '.[mujoco]'
    #pip install --default-timeout=100 -e '.[robotics]'
    
    cd ${SCRIPTS_DIR}

elif [ ${py_ver} == "3" ]
then

    # Make sure Python and pip are installed
    source ./install_python_core.sh ${py_ver}

    sudo apt-get install -y build-essential cmake python3-dev python3-opengl libboost-all-dev zlib1g-dev libsdl2-dev libav-tools xorg-dev libjpeg-dev swig
    
    #python3 -m pip install --default-timeout=100 --upgrade pip setuptools wheel    
    pip3 install --upgrade setuptools wheel
    
    pip3 install --user gym
    
    pip3 install --user 'gym[atari]'
    pip3 install --user 'gym[box2d]'
    pip3 install --user 'gym[classic_control]'
    
else
    
    echo -e "${ERROR_COLOR}Usage: ${0} [2|3]${NC}"
    echo -e "${ERROR_COLOR}Given arg: ${py_ver}${NC}"
    
fi

set +e
source ~/.bashrc
set -e


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing OpenAI Gym -----${NC}"
echo -e ""

trap - ERR
