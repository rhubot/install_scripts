#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# Some variables to make things easier to read
false=0
true=1

# Command line argument variables
ocvVersion=3


# A function that displays a help / usage message to the user
function displayHelp() {
    echo -e "Usage:"
    echo -e "    install_opencv.sh [options]"
    echo -e ""
    echo -e "Options:"
    echo -e "    --help, -h"
    echo -e "        Show this help message"
    echo -e "    --version <major version number>, -v <major version number>"
    echo -e "        Specify the version of OpenCV you wish to install. If not specified, version defaults to 3"
    echo -e "        Where <major version number> is the version of OpenCV (i.e. 3 or 4)"
    echo -e "        Example: install_opencv.sh --version 4"
    
    # Exit out of this script
    exit
}

# Process any command line args
while [ "${1}" != "" ]
do
    case ${1} in
        -h | --help )
            displayHelp
            ;;
        -v | --version )
            shift
            ocvVersion=${1}
            ;;
        * )
            echo -e "${WARNING_COLOR}Unknown option provided: ${1}${NC}"
            displayHelp
            ;;
    esac
    shift
done


echo -e ""
echo -e "${TITLE_COLOR}===== Installing OpenCV ${ocvVersion} =====${NC}"
echo -e ""


if [ "${ocvVersion}" == "3" ]
then
    echo -e "${ERROR_COLOR}This script is not yet implemented for OpenCV ${ocvVersion}${NC}"
    exit 1
elif [ "${ocvVersion}" == "4" ]
then
    echo -e "${ERROR_COLOR}This script is not yet implemented for OpenCV ${ocvVersion}${NC}"
    exit 1
else
    echo -e "${ERROR_COLOR}This script is not yet implemented for OpenCV ${ocvVersion}${NC}"
    exit 1
fi


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing OpenCV ${ocvVersion} -----${NC}"
echo -e ""

trap - ERR
