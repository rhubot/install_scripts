#!/bin/bash -i                                                                                                                          
# NOTE: This script was auto generated                                                                                                  
                                                                                                                                        
set -e                                                                                                                                  
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR 
                                                                                                                                        
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""                                                                                                 
                                                                                                                                        
# Specify yes/no values                                                                                                                 
affirm="y"                                                                                                                            
neg="n"                                                                                                                               
unknown="maybe"                                                                                                                       
                                                                                                                                        
sc_user=""                                                                                                                            
sc_email=""                                                                                                                           
                                                                                                                                        
# Set default values                                                                                                                    
choice_0=$affirm                                                                   
choice_1=$neg                                                                      
choice_2=$neg                                                                      
choice_3=$neg                                                                      
choice_done=$unknown                                                                              
                                                                                                   
# Set index values                                                                                 
index_done=0                                                                                       
index_0=1                                                                    
index_1=2                                                                    
index_2=3                                                                    
index_3=4                                                                    
                                                                                                                                 
# Menu variables                                                                                                                 
currentMenuIndex=1                                                                                                               
user_choice=$affirm                                                                                                             
default_choice=$unkonwn                                                                                                         
                                                                                                                                 
# Display the menu                                                                                                               
fullMenu()                                                                                                                       
{                                                                                                                                
    clear                                                                                                                        
    echo -e "${WARNING_COLOR}NOTE: If you are looking to setup an entire project, you may prefer to use setup_scripts${NC}"  
    echo ""                                                                                                                    
    echo ""                                                                                                                    
    echo "Which apps would you like to install?"                                                                               
    echo ""                                                                                                                    
                                                                                                                                 
    # Python 2.x                                                                                     
    if [ $currentMenuIndex -eq $index_0 ]                                             
    then                                                                                       
        default_choice=$choice_0                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_0"] Python 2.x${NC}"                     
    elif [ "$choice_0" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_0"] Python 2.x${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_0"] Python 2.x${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 2 - Anaconda                                                                                     
    if [ $currentMenuIndex -eq $index_1 ]                                             
    then                                                                                       
        default_choice=$choice_1                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_1"] Python 2 - Anaconda${NC}"                     
    elif [ "$choice_1" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_1"] Python 2 - Anaconda${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_1"] Python 2 - Anaconda${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 2 - Torch                                                                                     
    if [ $currentMenuIndex -eq $index_2 ]                                             
    then                                                                                       
        default_choice=$choice_2                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_2"] Python 2 - Torch${NC}"                     
    elif [ "$choice_2" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_2"] Python 2 - Torch${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_2"] Python 2 - Torch${NC}"                           
    fi                                                                                         
                                                                                               
    # Python 2 - Tensor Flow                                                                                     
    if [ $currentMenuIndex -eq $index_3 ]                                             
    then                                                                                       
        default_choice=$choice_3                                                       
        echo -e "${PROMPT_COLOR}--> ["$choice_3"] Python 2 - Tensor Flow${NC}"                     
    elif [ "$choice_3" == "$affirm" ]                                             
    then                                                                                       
        echo -e "${LIME}    ["$choice_3"] Python 2 - Tensor Flow${NC}"                             
    else                                                                                       
        echo -e "${MAROON}    ["$choice_3"] Python 2 - Tensor Flow${NC}"                           
    fi                                                                                         
                                                                                               
    # Done                                                                                         
    if [ $currentMenuIndex -eq $index_done ]                                                     
    then                                                                                           
        default_choice=$affirm                                                                    
        echo -e "${PROMPT_COLOR}--> [y] Done${NC}"                                             
    else                                                                                           
        echo "    [ ] Done"                                                                      
    fi                                                                                             
                                                                                                   
    echo ""                                                                                      
    echo "Menu options:"                                                                         
    echo "  'y' or 'n' to choose if you want to install the currently selected app"              
    echo "  [enter] to accept current value"                                                     
    echo "  'a' to install all listed apps"                                                      
    echo "  'd' to install the currently selected apps"                                          
    echo "  'q' to quit without installing anything"                                             
    echo "  'p' to move to the previously selected app"                                          
                                                                                                   
    # Read the user's choice                                                                       
    read user_choice                                                                               
                                                                                                   
    # Process the user's choice                                                                    
    if [ $user_choice == "a" ]                                                                  
    then                                                                                           
        echo -e "${TITLE_COLOR}Installing all apps${NC}"                                       
        choice_0=$affirm    
        choice_1=$affirm    
        choice_2=$affirm    
        choice_3=$affirm    
        choice_done=$affirm                                                                       
    elif [ $user_choice == "d" ]                                                                
    then                                                                                           
        echo -e "${TITLE_COLOR}Installing currently selected apps${NC}"                        
        choice_done=$affirm                                                                       
    elif [ $user_choice == "q" ]                                                                
    then                                                                                           
        echo -e "${WARNING_COLOR}Exiting without installing anything${NC}"                     
        choice_0=$neg    
        choice_1=$neg    
        choice_2=$neg    
        choice_3=$neg    
        choice_done=$affirm                                                                       
    elif [ $user_choice == "p" ]                                                                
    then                                                                                           
        user_choice=$default_choice                                                               
        decrementMenuIndex                                                                         
    elif [ $user_choice == "y" ]                                                                
    then                                                                                           
        updateChoice                                                                               
    elif [ $user_choice == "n" ]                                                                
    then                                                                                           
        updateChoice                                                                               
    else                                                                                           
        echo "----->"$user_choice"<-----"                                                     
        echo "Using default value"                                                               
        user_choice=$default_choice                                                               
                                                                                                   
        if [ $currentMenuIndex -eq $index_done ]                                                 
        then                                                                                       
            user_choice=$affirm                                                                   
        fi                                                                                         
        updateChoice                                                                               
    fi                                                                                             
}                                                                                                  
                                                                                                   
# Update the choice variables based on user input                                                  
updateChoice()                                                                                     
{                                                                                                  
    # Process user's y/n choice                                                                    
    if [ $currentMenuIndex -eq $index_0 ]                                                 
    then                                                                                           
        choice_0=$user_choice                                                              
        incrementMenuIndex                                                                         
    elif [ $currentMenuIndex -eq $index_1 ]                                           
    then                                                                                       
        choice_1=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_2 ]                                           
    then                                                                                       
        choice_2=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_3 ]                                           
    then                                                                                       
        choice_3=$user_choice                                                          
        incrementMenuIndex                                                                     
    elif [ $currentMenuIndex -eq $index_done ]                                                   
    then                                                                                           
        choice_done=$user_choice                                                                  
    else                                                                                           
        echo -e "${WARNING_COLOR}Menu index got messed up: ${NC}" $currentMenuIndex           
        choice_done=$affirm                                                                       
    fi                                                                                             
}                                                                                                  
                                                                                                   
# Decrement to the previous menu item                                                              
decrementMenuIndex()                                                                               
{                                                                                                  
    if [ $currentMenuIndex -eq $index_0 ]                                                 
    then                                                                                           
        currentMenuIndex=$index_0                                                          
    elif [ $currentMenuIndex -eq $index_1 ]                                           
    then                                                                                       
        currentMenuIndex=$index_0                                                  
    elif [ $currentMenuIndex -eq $index_2 ]                                           
    then                                                                                       
        currentMenuIndex=$index_1                                                  
    elif [ $currentMenuIndex -eq $index_3 ]                                           
    then                                                                                       
        currentMenuIndex=$index_2                                                  
    elif [ $currentMenuIndex -eq $index_done ]                                                   
    then                                                                                           
        currentMenuIndex=$index_3                                                      
    else                                                                                           
        currentMenuIndex=$index_done                                                              
    fi                                                                                             
}                                                                                                  
                                                                                                   
# Increment to the next menu item                                                                  
incrementMenuIndex()                                                                               
{                                                                                                  
    if [ $currentMenuIndex -eq $index_0 ]                                                 
    then                                                                                           
        currentMenuIndex=$index_1                                                      
    elif [ $currentMenuIndex -eq $index_1 ]                                           
    then                                                                                       
        currentMenuIndex=$index_2                                                  
    elif [ $currentMenuIndex -eq $index_2 ]                                           
    then                                                                                       
        currentMenuIndex=$index_3                                                  
    elif [ $currentMenuIndex -eq $index_3 ]                                               
    then                                                                                           
        currentMenuIndex=$index_done                                                              
    else                                                                                           
        currentMenuIndex=$index_done                                                              
    fi                                                                                             
}                                                                                                  
                                                                                                   
# Loop thorugh the menu until done                                                                 
loop()                                                                                             
{                                                                                                  
    fullMenu                                                                                       
                                                                                                   
    if [ $choice_done == $affirm ]                                                               
    then                                                                                           
        echo -e "${FINISHED_TITLE_COLOR}Done${NC}"                                             
    elif [ $choice_done == $neg ]                                                                
    then                                                                                           
        currentMenuIndex=1                                                                         
        choice_done=$unknown                                                                      
        loop                                                                                       
    else                                                                                           
        loop                                                                                       
    fi                                                                                             
}                                                                                                  
                                                                                                  
# When installing source control, we will need more info                                          
getSourceControlParams()                                                                          
{                                                                                                 
    read -p "$(echo -e ${PROMPT_COLOR}"Username for source control: "${NC})" sc_user       
    read -p "$(echo -e ${PROMPT_COLOR}"Email for source control: "${NC})" sc_email         
}                                                                                                 
                                                                                                  
                                                                                                   
                                                                                                   
# Start the menu loop                                                                              
loop                                                                                               
                                                                                                   
# Install stuff                                                                                    
if [ $user_choice != "q" ]                                                                      
then                                                                                               
    clear                                                                                          
    getSourceControlParams                                                                         
    clear                                                                                          
    echo -e ""                                                                                   
    echo -e "${TITLE_COLOR}~~~~~ Installing apps ~~~~~${NC}"                                   
    echo -e ""                                                                                   
    sudo apt-get update                                                                            
                                                                                                   
                                                                                                   
    # Install the selected apps                                                                    
                                                                                               
if [ "${choice_0,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_core.sh 2                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_1,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_anaconda.sh 2                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_2,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_pytorch.sh 2                                                                          
fi                                                                                             
                                                                                               
if [ "${choice_3,,}" == "${affirm,,}" ]                                           
then                                                                                           
    source ${INSTALL_SCRIPTS_DIR}/install_python_tensorflow.sh 2                                                                          
fi                                                                                             
                                                                                                   
    echo -e ""                                                                                   
    echo -e "${FINISHED_TITLE_COLOR}***** Finished installing apps *****${NC}"                 
    echo -e ""                                                                                   
fi                                                                                                 
                                                                                                   
trap - ERR                                                                                         
