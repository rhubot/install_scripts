#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

# Make sure we are given the proper number of command line arguments
if test $# -ne 1
then
    echo -e "${ERROR_COLOR}Usage: ${0} [2|3]${NC}"
    echo -e "${ERROR_COLOR}Given $# command line arguments${NC}"
    exit 1
fi

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Anaconda =====${NC}"
echo -e ""

# Get the command line arguments
py_ver=${1}

if [ ${py_ver} == "2" ]
then
    
    echo -e "${WARNING_COLOR}This might be wrong...${NC}"
    wget http://repo.continuum.io/archive/Anaconda3-4.3.0-Linux-x86_64.sh -O ~/anaconda.sh
    
elif [ ${py_ver} == "3" ]
then
    
    wget https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh -O ~/anaconda.sh
    
else

    echo -e "${ERROR_COLOR}Usage: ${0} [2|3]${NC}"
    echo -e "${ERROR_COLOR}Given arg: ${py_ver}${NC}"
    
fi

# Install Anaconda
if [ -f ~/anaconda.sh ]
then
    chmod 755 ~/anaconda.sh 
    ~/anaconda.sh -b -f -p $HOME/anaconda
    echo "export PATH=$HOME/anaconda/bin:$PATH" >> ~/.bashrc
fi


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing Anaconda -----${NC}"
echo -e ""

trap - ERR
