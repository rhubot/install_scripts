#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

# Make sure we are given the proper number of command line arguments
if test $# -ne 1
then
    echo -e "${ERROR_COLOR}Usage: ${0} [2|3]${NC}"
    echo -e "${ERROR_COLOR}Given $# command line arguments${NC}"
    exit 1
fi

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Pytorch =====${NC}"
echo -e ""

# Get the command line arguments
py_ver=${1}

if [ ${py_ver} == "2" ]
then

    # Links obtained from https://pytorch.org/
    pip install --default-timeout=100 http://download.pytorch.org/whl/cpu/torch-0.4.1-cp35-cp35m-linux_x86_64.whl
    pip install --default-timeout=100 torchvision

elif [ ${py_ver} == "3" ]
then

    pip3 install --user torch torchvision
    pip3 install --user tensorboardX
    
else

    echo -e "${ERROR_COLOR}Usage: ${0} [2|3]${NC}"
    echo -e "${ERROR_COLOR}Given arg: ${py_ver}${NC}"
    
fi

sudo apt-get install -y gfortran pkg-config software-properties-common


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing Pytorch -----${NC}"
echo -e ""

trap - ERR
