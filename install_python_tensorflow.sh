#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

# Make sure we are given the proper number of command line arguments
if test $# -ne 1
then
    echo -e "${ERROR_COLOR}Usage: ${0} [2|3]${NC}"
    echo -e "${ERROR_COLOR}Given $# command line arguments${NC}"
    exit 1
fi

echo -e ""
echo -e "${TITLE_COLOR}===== Installing TensorFlow =====${NC}"
echo -e ""

# Get the command line arguments
py_ver=${1}

if [ ${py_ver} == "2" ]
then

    #sudo apt install -y python-pip
    #pip install --default-timeout=100 --upgrade pip
    pip install --default-timeout=100 --user tensorflow

elif [ ${py_ver} == "3" ]
then

    #pip3 install --upgrade pip
    pip3 install --user tensorflow
    
else

    echo -e "${ERROR_COLOR}Usage: ${0} [2|3]${NC}"
    echo -e "${ERROR_COLOR}Given arg: ${py_ver}${NC}"
    
fi

echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing TensorFlow -----${NC}"
echo -e ""

trap - ERR
