#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

VER=$(lsb_release -sr)

# Remember where we started
SCRIPTS_DIR=$(pwd)

cd ${INSTALL_SCRIPTS_DIR}

if [ $VER == "22.04" ]
then
    source ./install_ros2_humble.sh
#elif [ $VER == "16.04" ] || [ $VER == "15.10" ]
#then
#    source ./install_ros_kinetic.sh
#elif [ $VER == "18.04" ] || [ $VER == "17.10" ]
#then
#    source ./install_ros_melodic.sh
#elif [ $VER == "20.04" ]
#then
#    source ./install_ros_noetic.sh
else
    echo -e "${ERROR_COLOR}Script not yet implemented for this version of Ubuntu/ROS2${NC}"
fi

# Return to where we started
cd ${SCRIPTS_DIR}

trap - ERR
