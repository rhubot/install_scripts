#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing ROS2 Humble =====${NC}"
echo -e ""


VER=$(lsb_release -sr)

if [ $VER == "22.04" ]
then
    LOC=$(locale)
    #echo -e "Loc: ${LOC}"
    if [[ $LOC != *"UTF-8"* ]]
    then
        echo "Locale should be UTF-8. Attempting to fix..."
        sudo apt update && sudo apt install -y locales
        sudo locale-gen en_US en_US.UTF-8
        sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
        export LANG=en_US.UTF-8
    fi
    
    ## You will need to add the ROS 2 apt repository to your system.
    
    # First ensure that the Ubuntu Universe repository is enabled.
    sudo apt install -y software-properties-common
    sudo add-apt-repository universe    
    
    # Now add the ROS 2 GPG key with apt.
    sudo apt update && sudo apt install -y curl
    sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg
    
    # Then add the repository to your sources list.
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null
    
    ##Install ROS 2 packages
    
    # Update your apt repository caches after setting up the repositories.
    sudo apt update
    
    # ROS 2 packages are built on frequently updated Ubuntu systems. It is always recommended that you ensure your system is up to date before installing new packages.
    sudo apt upgrade
    
    # Desktop Install (Recommended): ROS, RViz, demos, tutorials.
    sudo apt install -y ros-humble-desktop-full
    
    # Development tools: Compilers and other tools to build ROS packages
    sudo apt install -y ros-dev-tools
    
    ## Environment setup
    
    # Add to your .bashrc file
    echo "source /opt/ros/humble/setup.bash" >> ~/.bashrc
    echo "export ROS_DOMAIN_ID=97" >> ~/.bashrc
    #echo "export ROS_LOCALHOST_ONLY=1" >> ~/.bashrc  ## Generally not needed, but here just in case

    # Sourcing the setup script. Set up your environment by sourcing the following file.
    set +e
    trap - ERR
    source /opt/ros/humble/setup.bash
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
else
    echo -e "${ERROR_COLOR}ROS2 Humble requires Ubuntu 22.04${NC}"
fi


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing ROS2 Humble -----${NC}"
echo -e ""

trap - ERR
