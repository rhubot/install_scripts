#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing ROS Melodic =====${NC}"
echo -e ""


VER=$(lsb_release -sr)

if [ $VER == "18.04" ] || [ $VER == "17.10" ]
then
    
    # Setup your computer to accept software from packages.ros.org
    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    
    # Set up your keys
    sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
    
    # Update
    sudo apt-get update
    
    # Desktop-Full Install
    sudo apt install -y ros-melodic-desktop-full
    
    # Before you can use ROS, you will need to initialize rosdep. rosdep enables you to easily install system dependencies for source you want to compile and is required to run some core components in ROS
    if [[ ! -f /etc/ros/rosdep/sources.list.d/20-default.list ]]
    then
        sudo apt install python-rosdep
        sudo rosdep init
    fi
    echo "Running rosdep update"
    rosdep update
    
    # ROS environment variables are automatically added to your bash session
    echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
    set +e
    trap - ERR
    source ~/.bashrc
    set -e
    trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR
    
    # Various tools and requirements to create and manage your own ROS workspaces
    sudo apt install -y python-rosinstall python-rosinstall-generator python-wstool build-essential

else
    echo -e "${ERROR_COLOR}ROS Melodic requires Ubuntu 18.04 or 17.10${NC}"
fi


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing ROS Melodic -----${NC}"
echo -e ""

trap - ERR
