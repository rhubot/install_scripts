#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing MoveIt =====${NC}"
echo -e ""


VER=$(lsb_release -sr)

if [ $VER == "14.04" ] || [ $VER == "13.10" ]
then
    # For ROS Indigo
    sudo apt-get install ros-indigo-moveit
    
elif [ $VER == "16.04" ] || [ $VER == "15.10" ]
then
    ## For ROS Kinetic
    sudo apt-get install -y ros-kinetic-moveit
    sudo apt-get install -y ros-kinetic-moveit-core ros-kinetic-moveit-kinematics ros-kinetic-moveit-ros-planning ros-kinetic-moveit-ros-move-group ros-kinetic-moveit-planners-ompl ros-kinetic-moveit-ros-visualization ros-kinetic-moveit-simple-controller-manager ros-kinetic-moveit-visual-tools
    
elif [ $VER == "18.04" ] || [ $VER == "17.10" ]
then
    # For ROS Melodic
    sudo apt install -y ros-melodic-moveit
    sudo apt-get install -y ros-melodic-moveit-core ros-melodic-moveit-kinematics ros-melodic-moveit-ros-planning ros-melodic-moveit-ros-move-group ros-melodic-moveit-planners-ompl ros-melodic-moveit-ros-visualization ros-melodic-moveit-simple-controller-manager ros-melodic-moveit-visual-tools
    
else
    echo -e "${ERROR_COLOR}Script not yet implemented for this version of Ubuntu/MoveIt${NC}"
fi


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing MoveIt -----${NC}"
echo -e ""

trap - ERR
