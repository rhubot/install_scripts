#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

# Remember where we started
SCRIPTS_DIR=$(pwd)

# Get color definitions
source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing setup_scripts Tool =====${NC}"
echo -e ""


sc_user=""
sc_email=""

# Process any command line args
while [ "${1}" != "" ]
do
     case ${1} in
        -u | --sc_user )
            shift
            sc_user=${1}
            ;;
        -e | --sc_email )
            shift
            sc_email=${1}
            ;;
        * )
            echo -e "${WARNING_COLOR}Unknown option provided: ${1}${NC}"
            ;;
    esac
    shift
done

# Make sure we are given the proper command line arguments
if [ "${sc_user}" == "" ] || [ "${sc_email}" == "" ]
then
    # Prompt the user for the info we need
    read -p "$(echo -e ${PROMPT_COLOR}"Username for source control: "${NC})" sc_user
    #read -p "$(echo -e ${PROMPT_COLOR}"Email for source control: "${NC})" sc_email
fi

# Clone the repo
if [ ! -d ~/setup_scripts/ ]
then
    git clone https://${sc_user}@bitbucket.org/rhubot/setup_scripts.git ~/setup_scripts
else
    echo -e "${WARNING_COLOR}~/setup_scripts already exists${NC}"
fi


# Set the environment variable so other scripts know where to find the setup_scripts directory
if [ "${SETUP_SCRIPTS_DIR}" == "" ]
then
    cd ~/setup_scripts
    source ./configure_setup_scripts.sh
fi

# Return from where we came
cd ${SCRIPTS_DIR}


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing setup_scripts Tool -----${NC}"
echo -e ""

trap - ERR
