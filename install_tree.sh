#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Tree =====${NC}"
echo -e ""

sudo apt-get install -y tree

echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing Tree -----${NC}"
echo -e ""

trap - ERR
