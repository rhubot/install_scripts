#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing Valgrind =====${NC}"
echo -e ""

sudo apt-get install -y valgrind

echo ""
echo "${FINISHED_TITLE_COLOR}----- Finished installing Valgrind -----${NC}"
echo ""

trap - ERR
