#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# Some variables to make things easier to read
false=0
true=1

# Command line argument variables
ros_1=${false}


# A function that displays a help / usage message to the user
function displayHelp() {
    echo -e "Usage:"
    echo -e "    install_webots.sh [options]"
    echo -e ""
    echo -e "Options:"
    echo -e "    --help, -h"
    echo -e "        Show this help message"
    echo -e "    --ros-1, -1"
    echo -e "        Use ROS1 instead of the default ROS2"
    
    # Exit out of this script
    exit
}

# Process any command line args
while [ "${1}" != "" ]
do
    case ${1} in
        -h | --help )
            displayHelp
            ;;
        -1 | --ros-1 )
            ros_1=${true}
            ;;
        * )
            echo -e "${WARNING_COLOR}Unknown option provided: ${1}${NC}"
            displayHelp
            ;;
    esac
    shift
done


echo -e ""
echo -e "${TITLE_COLOR}===== Installing WeBots =====${NC}"
echo -e ""


# Install WeBots
if [ ${ros_1} == ${false} ]
then
    source /opt/ros/humble/setup.bash
    sudo apt-get -y install ros-$ROS_DISTRO-webots-ros2
else
    echo -e "${ERROR_COLOR}This script is not yet implemented for ROS1${NC}"
fi


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing WeBots -----${NC}"
echo -e ""

trap - ERR
