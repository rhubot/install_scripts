#!/bin/bash -i
set -e
trap 'source ${INSTALL_SCRIPTS_DIR}/attempt_fix_on_err.sh ${BASH_LINENO} ${0} ${LINENO} ${BASH_ARGC} ${BASH_ARGV} ${BASH_COMMAND}' ERR

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""

echo -e ""
echo -e "${TITLE_COLOR}===== Installing WireShark =====${NC}"
echo -e ""


sudo DEBIAN_FRONTEND=noninteractive apt-get -y install wireshark
#echo "wireshark-common wireshark-common/install-setuid boolean true" | sudo debconf-set-selections
#sudo DEBIAN_FRONTEND=noninteractive dpkg-reconfigure wireshark-common


echo -e ""
echo -e "${FINISHED_TITLE_COLOR}----- Finished installing WireShark -----${NC}"
echo -e ""

trap - ERR
