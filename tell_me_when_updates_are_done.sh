#!/bin/bash -i

source ${INSTALL_SCRIPTS_DIR}/colors.sh ""


# This section based on a script from http://invisible-island.net/xterm/xterm.faq.html
exec < /dev/tty
oldstty=$(stty -g)
stty raw -echo min 0
# on my system, the following line can be replaced by the line below it
echo -en "\033[6n" > /dev/tty
# tput u7 > /dev/tty    # when TERM=xterm (and relatives)
IFS=';' read -r -d R -a pos
stty $oldstty
# change from one-based to zero based so they work with: tput cup $row $col
row=$((${pos[0]:2} - 1))    # strip off the esc-[
col=$((${pos[1]} - 1))
#echo "(row,col): $row,$col"


# Call the ps command
PS_CMD=$(ps -aux | grep apt)
INDEX=1

# Check to see if the lock is held
#while [[ ${INDEX} -le 3 ]]   # Left here for future testing
while [[ ${PS_CMD} == *"/usr/lib/apt/apt.systemd.daily lock_is_held"* ]]
do
    # Set the cursor position
    tput cup ${row} 5
    
    # Display waiting message
    if [ ${INDEX} -le 1 ]
    then
        echo -e "${WARNING_COLOR}Waiting for apt-get lock     ${NC}"
    elif [ ${INDEX} -le 2 ]
    then
        echo -e "${WARNING_COLOR}Waiting for apt-get lock.${NC}"
    elif [ ${INDEX} -le 3 ]
    then
        echo -e "${WARNING_COLOR}Waiting for apt-get lock..${NC}"
    else
        echo -e "${WARNING_COLOR}Waiting for apt-get lock...${NC}"
        INDEX=0
    fi
    
    # Are we there yet?
    INDEX=$(( ${INDEX} + 1 ))
    PS_CMD=$(ps -aux | grep apt)
    sleep 1
done

tput cup ${row} 5
echo -e "${LIME}The apt-get lock is now available${NC}"
echo -e ""
echo -e ""
